package com.ugat77.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Table(name = Contract.TBL_NAME)
public class Contract extends AbstractEntity<Long> {

    public static final String TBL_NAME = "contract";
    public static final String FLD_START_DATE = "start_date";
    public static final String FLD_END_DATE = "end_date";
    public static final String FLD_EMPLOYEE = "employee";

    @NotNull
    @Column(name = FLD_START_DATE, nullable = false)
    private LocalDateTime start_date;

    @NotNull
    @Column(name = FLD_END_DATE, nullable = false)
    private LocalDateTime end_date;

    @OneToOne
    @JoinColumn(name = FLD_EMPLOYEE)
    private Employee employee;
}
