package com.ugat77.controller;

import com.ugat77.entity.User;
import com.ugat77.service.UserService;
import com.ugat77.utils.VerifyCodeUtils;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
public class RegisterController {

    private UserService userService;

    @Autowired
    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("user") User user, String code, HttpSession session) {
        try {
            String sesssionCode = session.getAttribute("code").toString();
            if (!sesssionCode.equalsIgnoreCase(code)) throw new RuntimeException("Invalid code");
            userService.save(user);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return "register";
        }
        return "redirect:/login";
    }

   @RequestMapping("register/generateImageCode")
    public void generateImageCode(HttpSession session, HttpServletResponse response) throws IOException {
        //generate 4 digits random characters
        String code = VerifyCodeUtils.generateVerifyCode(4);
        session.setAttribute("code",code);

        //generate image, response image and set response type
        response.setContentType("image/png");
        ServletOutputStream os = response.getOutputStream();
        VerifyCodeUtils.outputImage(220,60, os, code);
    }

}
