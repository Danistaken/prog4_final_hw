package com.ugat77.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = Project.TBL_NAME)
public class Project extends AbstractEntity<Long>{

    public static final String TBL_NAME = "project";
    public static final String FLD_NAME = "name";
    public static final String FLD_OFFICE = "offices";

    @Column(name = FLD_NAME, nullable = false)
    private String projectName;

    @ManyToMany
    @JoinTable(
            name = "project_office",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "office_id")
    )
    private List<Office> offices;

}
