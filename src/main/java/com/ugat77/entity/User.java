package com.ugat77.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = User.TBL_NAME)
public class User extends AbstractEntity<Long>{

    public static final String TBL_NAME = "users";
    public static final String FLD_NAME = "username";
    public static final String FLD_PASSWORD = "password";
    public static final String FLD_EMAIL = "email";

    @Column(name = FLD_NAME, unique = true, nullable = false)
    @NotNull(message = "Username cannot be null")
    private String username;

    @Column(name = FLD_PASSWORD, nullable = false)
    @NotNull(message = "Password cannot be null")
    private String password;

    @Column(name = FLD_EMAIL, nullable = false)
    @Email(message = "Invalid email address")
    @NotNull(message = "Email cannot be null")
    private String email;
}
