package com.ugat77.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.ui.Model;
import com.ugat77.entity.User;
import com.ugat77.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

        private UserService userService;

        @Autowired
        public LoginController(UserService userService) {
            this.userService = userService;
        }

        @GetMapping({"login"})
        public String showLoginForm(Model model, HttpSession session) {
            //clear session
            session.invalidate();
            model.addAttribute("user", new User());
            return "login";
        }

        @RequestMapping("/register")
        public String register() {
            return "register";
        }

        @PostMapping("/login")
        public String login(@ModelAttribute("user") User user,Model model, HttpSession session) {
            User authenticatedUser = userService.authenticate(user.getUsername(), user.getPassword());
            if (authenticatedUser != null) {
                session.setAttribute("user", authenticatedUser);
                return "redirect:/employee";
            } else {
                return "login";
            }
        }


}
