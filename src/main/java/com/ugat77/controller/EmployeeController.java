package com.ugat77.controller;

import com.ugat77.entity.Employee;
import com.ugat77.service.EmployeeService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("employee")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public String getAllEmployee(Model model) {
        model.addAttribute("employee", employeeService.findAll());
        return "employee";
    }

    //add employee
    @GetMapping("/{id}")
    public String getEmployeeById(@PathVariable("id") Long id, Model model) {
        Employee employee = employeeService.getById(id);
        model.addAttribute("employee", employee);
        return "employee";
    }

    @GetMapping("/new")
    public String addEmployeeForm(Model model) {
        model.addAttribute("employee", new Employee());
        return "addEmployee";
    }

    @PostMapping("/add")
    public String addEmployee(@ModelAttribute("employee") Employee employee) {
        employeeService.save(employee);
        return "redirect:/employee";
    }



    @GetMapping("/{id}/edit")
    public String editEmployee(@PathVariable("id") Long id, Model model, HttpSession session) {
        if (session.getAttribute("user") == null) {
            return "redirect:/login";
        }
        model.addAttribute("employee", employeeService.getById(id));
        return "editEmployee";
    }

    @PostMapping("/{id}/edit")
    public String updateEmployee(@PathVariable("id") Long id, @ModelAttribute("employee") Employee employee,
                                 Model model) {
        Employee existingEmployee = employeeService.getById(id);
        existingEmployee.setName(employee.getName());
        existingEmployee.setDepartment(employee.getDepartment());
        existingEmployee.setSalary(employee.getSalary());

        employeeService.save(existingEmployee);
        return "redirect:/employee";
    }

    @GetMapping("/{id}/delete")
    public String deleteEmployee(@PathVariable("id") Long id, HttpSession session) {
        if (session.getAttribute("user") == null) {
            return "redirect:/login";
        }
        employeeService.delete(id);
        return "redirect:/employee";
    }
}
