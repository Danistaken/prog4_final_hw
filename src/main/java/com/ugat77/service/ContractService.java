package com.ugat77.service;

import com.ugat77.entity.Contract;
import com.ugat77.repository.ContractRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractService {

    private final ContractRepository contractRepository;

    @Autowired
    public ContractService(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    public Contract save(Contract contract) {
        return contractRepository.save(contract);
    }

    public Contract getById(Long id) {
        return contractRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Contract not found " + id));
    }

    public List<Contract> findAll() {
        return contractRepository.findAll();
    }

    public void delete(Long id) {
        contractRepository.deleteById(id);
    }
}
