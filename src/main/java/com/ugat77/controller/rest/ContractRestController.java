package com.ugat77.controller.rest;

import com.ugat77.entity.Contract;
import com.ugat77.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contract")
public class ContractRestController {
    private final ContractService contractService;

    public ContractRestController(@Autowired ContractService cs) {
        contractService = cs;
    }

    @GetMapping
    public List<Contract> getAllContracts() {
        return contractService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Contract> getContractById(@PathVariable("id") Long id) {
        Contract contract = contractService.getById(id);

        if (contract == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(contract);
    }

    @PostMapping
    public ResponseEntity<Contract> createContract(@RequestBody Contract contract) {
        Contract createdContract = contractService.save(contract);
        return ResponseEntity.ok(createdContract);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Contract> updateContract(@PathVariable("id") Long id, @RequestBody Contract contract) {
        Contract existingContract = contractService.getById(id);
        if (existingContract == null) {
            return ResponseEntity.notFound().build();
        }
        contract.setId(id);
        Contract updatedContract = contractService.save(contract);
        return ResponseEntity.ok(updatedContract);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteContract(@PathVariable("id") Long id) {
        Contract contract = contractService.getById(id);
        if (contract == null) {
            return ResponseEntity.notFound().build();
        }
        contractService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

