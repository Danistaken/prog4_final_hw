package com.ugat77.config;

import com.ugat77.interceptor.AuthenticationInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registration) {
        registration.addInterceptor(new AuthenticationInterceptor()).addPathPatterns("/employee/edit/**");
    }
}
