package com.ugat77.controller.rest;

import com.ugat77.entity.Office;
import com.ugat77.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/office")
public class OfficeRestController {
    private final OfficeService officeService;

    public OfficeRestController(@Autowired OfficeService cs) {
        officeService = cs;
    }

    @GetMapping
    public List<Office> getAllOffices() {
        return officeService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable("id") Long id) {
        Office office = officeService.getById(id);

        if (office == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(office);
    }

    @PostMapping
    public ResponseEntity<Office> createOffice(@RequestBody Office office) {
        Office createdOffice = officeService.save(office);
        return ResponseEntity.ok(createdOffice);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Office> updateOffice(@PathVariable("id") Long id, @RequestBody Office office) {
        Office existingOffice = officeService.getById(id);
        if (existingOffice == null) {
            return ResponseEntity.notFound().build();
        }
        office.setId(id);
        Office updatedOffice = officeService.save(office);
        return ResponseEntity.ok(updatedOffice);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOffice(@PathVariable("id") Long id) {
        Office office = officeService.getById(id);
        if (office == null) {
            return ResponseEntity.notFound().build();
        }
        officeService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
