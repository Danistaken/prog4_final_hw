package com.ugat77;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class P4SpringHwApplication {

    public static void main(String[] args) {
        SpringApplication.run(P4SpringHwApplication.class, args);
    }

}
