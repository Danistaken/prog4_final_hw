package com.ugat77.service;

import com.ugat77.entity.Office;
import com.ugat77.repository.OfficeRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfficeService {

    private final OfficeRepository officeRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public Office save(Office office) {
        return officeRepository.save(office);
    }

    public Office getById(Long id) {
        return officeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Office not found " + id));
    }

    public List<Office> findAll() {
        return officeRepository.findAll();
    }

    public void delete(Long id) {
        officeRepository.deleteById(id);
    }
}