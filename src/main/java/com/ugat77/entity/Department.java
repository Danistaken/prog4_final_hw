package com.ugat77.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = Department.TBL_NAME)
public class Department extends AbstractEntity<Long> {

    public static final String TBL_NAME = "department";
    public static final String FLD_DEPTNAME = "name";
    public static final String FLD_OFFICE = "offices";

    @Column(name = FLD_DEPTNAME, nullable = false)
    @NotNull(message = "Name cannot be null")
    private String name;

    @OneToMany(mappedBy = "department")
    private List<Employee> employees;

    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    private List<Office> offices;

}
