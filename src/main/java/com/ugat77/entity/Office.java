package com.ugat77.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Entity
@Table(name = Office.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Office extends AbstractEntity<Long> {

    public static final String TBL_NAME = "office";
    public static final String FLD_NAME = "name";
    public static final String FLD_EMPLOYEE = "employees";
    public static final String FLD_PROJECT = "projects";
    public static final String FLD_DEPARTMENT = "department";

    @NotNull(message = "Name connot be null")
    @Column(name = FLD_NAME, nullable = false)
    private String name;

    @OneToMany(mappedBy = "office")
    private List<Employee> employees;

    @ManyToMany(mappedBy = "offices")
    private List<Project> projects;

    @ManyToOne
    @JoinColumn(name = FLD_DEPARTMENT)
    private Department department;
}
