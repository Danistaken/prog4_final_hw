-- employee
insert into employee (name, salary) values
                                                                  ('John Doe', 5000),
                                                                  ('Jane Smith', 6000),
                                                                  ('Mike Johnson', 4500);

-- office
insert into office (name) values
                              ('Office A'),
                              ('Office B');

-- department
insert into department (name) values
                                  ('Department X'),
                                  ('Department Y');

-- project
insert into project (name) values
                               ('Project 1'),
                               ('Project 2');

-- contract
insert into contract (start_date, end_date) values
                                                             ('2023-01-01', '2023-12-31'),
                                                             ('2022-07-01', '2022-12-31'),
                                                             ('2023-02-01', '2023-11-30');

-- user
insert into users (username, password,email)
values ('admin','admin','123@tr.com');
