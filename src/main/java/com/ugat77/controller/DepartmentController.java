package com.ugat77.controller;

import com.ugat77.entity.Department;
import com.ugat77.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("department")
public class DepartmentController {
    private DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping
    public String getAllDepartment(Model model) {
        model.addAttribute("department", departmentService.findAll());
        return "department";
    }

    //add department
    @GetMapping("/{id}")
    public String getDepartmentById(@PathVariable("id") Long id, Model model) {
        Department department = departmentService.getById(id);
        model.addAttribute("department", department);
        return "department";
    }


    @PostMapping("/new")
    public String addDepartment(@ModelAttribute("department") Department department) {
        departmentService.save(department);
        return "redirect:/department";
    }

    @GetMapping("/department/{id}")
    public String deleteDepartment(@PathVariable("id") Long id) {
        departmentService.delete(id);
        return "redirect:/department";
    }
}
