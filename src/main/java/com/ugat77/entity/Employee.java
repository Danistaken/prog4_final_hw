package com.ugat77.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Entity
@Table(name = Employee.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Employee extends AbstractEntity<Long>{

    public static final String TBL_NAME = "employee";
    public static final String FLD_NAME =  "name";
    public static final String FLD_SALARY = "salary";
//    public static final String FLD_BIRTHDAY = "birthday";
    public static final String FLD_DEPARTMENT = "department";
    public static final String FLD_OFFICE = "office";

    @JsonProperty("full_name")
    @NotNull(message = "Name cannot be empty")
    @Column(name = FLD_NAME, nullable = false)
    private String name;

    @JsonProperty("salary")
    @Column(name = FLD_SALARY)
    private double salary;

    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
    private Contract contract;

    @ManyToOne
    @JoinColumn(name = FLD_OFFICE)
    private Office office;

    @ManyToOne
    @JoinColumn(name = FLD_DEPARTMENT)
    private Department department;

}
